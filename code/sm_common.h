#ifndef SM_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#define Kilobytes(Value) (Value * 1024LL)
#define Megabytes(Value) (Kilobytes(Value) * 1024LL)

typedef int bool32;
typedef float real32;
typedef double real64;

#include "sm_internet.h"

struct internet
{
	HMODULE Library;

	init_internet *InitInternet;
	close_internet *CloseInternet;

	create_internet_request *CreateInternetRequest;
	destroy_internet_request *DestroyInternetRequest;

	internet_request_get_results *InternetRequestGetResults;
};

#ifdef __cplusplus
}
#endif

#define SM_COMMON_H
#endif
