#include "sm_texttools.h"

int
StringLength(char *String)
{
	int Result = 0;

	while(*String++ != 0)
	{
		++Result;
	}

	return(Result);
}

int 
IndexOfKeyword(char *Keyword, char *Source, int Length, int StartIndex = 0)
{
	int Index = -1;
	char *SourceStart = Source;
	Source = Source + StartIndex;

	while(*Source != 0)
	{
		if(*Source == *Keyword)
		{
			int KeywordIndex = 0;
			for(;
				Source[KeywordIndex] == Keyword[KeywordIndex];
				++KeywordIndex);

			if(Keyword[KeywordIndex] == 0)
			{
				Index = Source - SourceStart;
				break;
			}
		}

		++Source;
	}

	return(Index);
}

text_item
GetNextNumber(char *Source, int Length, int StartIndex = 0)
{
	text_item Result;
	Result.Type = TextItemType_Number;

	char *SourceBegin = Source;
	Source = Source + StartIndex;
	while((*Source != 0) &&
		  (!IsNumber(*Source)))
	{
		++Source;
	}

	char *MatchStart = Source;
	if(*Source != 0)
	{
		Result.Index = Source - SourceBegin;
		Result.Text = Source;

		while((*Source != 0) &&
			  (IsNumber(*Source)))
		{
			++Source;
		}

		Result.Length = Source - MatchStart;
	}

	return(Result);
}

text_item
GetString(char *Keyword, char *Source, int Length, int StartIndex = 0)
{
	text_item Result;
	Result.Type = TextItemType_String;

	int Index = IndexOfKeyword(Keyword, Source, Length, StartIndex);

	if(Index != -1)
	{
		Result.Text = Source + Index;
		Result.Length = StringLength(Keyword);
		Result.Index = Index;
	}

	return(Result);
}

// InsideString - If true, we are inside of a string and we should search
// forward and backward to find the strings limits.
text_item
GetInsideString(char *Source, int Index, int Length, bool32 InsideString = 0)
{
	char *SourceStart = Source;

	Source = Source + Index;
	char *StringStart = Source;
	if(InsideString && Index > 0)
	{
		while(StringStart > Source)
		{
			if(*StringStart == '"')
			{
				break;
			}

			--StringStart;
		}
	}

	char *StringEnd = Source;
	if(*StringStart == '"')
	{
		while(*StringEnd != 0)
		{
			if(*StringEnd == '"')
			{
				break;
			}

			++StringEnd;
		}
	}

	text_item Result;
	Result.Type = TextItemType_String;
	Result.Text = StringStart;
	Result.Length = StringEnd - StringStart;
	Result.Index = StringStart - SourceStart;

	return(Result);
}

text_item
GetAttribute(char *Keyword, char *Source, int Length, int StartIndex = 0)
{
	text_item Result;
	Result.Type = TextItemType_String;
	char *SourceStart = Source;
	Source = Source + StartIndex;

	text_item Tag = GetString(Keyword, Source, StartIndex);
	if(Tag.Index)
	{
		int ValueIndex = IndexOfKeyword("\"", Source, Length, Tag.Index);
		if(ValueIndex != -1)
		{
			char *StringStart = Source + (ValueIndex + 1);
			char *StringEnd = StringStart;
			while(*StringEnd != 0)
			{
				if(*StringEnd == '"')
				{
					break;
				}
				++StringEnd;
			}

			Result.Text = StringStart;
			Result.Index = StringStart - SourceStart;
			Result.Length = StringEnd - StringStart;
		}
	}

	return(Result);
}
