#ifndef SM_TEXTTOOLS_H

enum text_item_type
{
	TextItemType_String,
	TextItemType_Number,
};

struct text_item
{
	text_item_type Type;
	char *Text;
	int Length;
	int Index;
};

#define IsNumber(Character) ((Character >= '0') && (Character <= '9'))

#define SM_TEXTTOOLS_H
#endif
