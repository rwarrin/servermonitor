#ifndef SM_INTERNET_H

#ifdef __cplusplus
extern "C" {
#endif

struct internet_request_result
{
	void *Data;
	DWORD Length;
	DWORD MaxLength;
	int Status;
	HINTERNET RequestHandle;
};

#define INIT_INTERNET(Name) HINTERNET Name(void)
typedef INIT_INTERNET(init_internet);

#define CLOSE_INTERNET(Name) void Name(HINTERNET InternetHandle)
typedef CLOSE_INTERNET(close_internet);

#define CREATE_INTERNET_REQUEST(Name) internet_request_result Name(HINTERNET InternetHandle, char *Url, DWORD MaxSize)
typedef CREATE_INTERNET_REQUEST(create_internet_request);

#define DESTROY_INTERNET_REQUEST(Name) void Name(internet_request_result *InternetRequestResult)
typedef DESTROY_INTERNET_REQUEST(destroy_internet_request);

#define INTERNET_REQUEST_GET_RESULTS(Name) void Name(internet_request_result *RequestResult)
typedef INTERNET_REQUEST_GET_RESULTS(internet_request_get_results);

#ifdef __cplusplus
}
#endif

#define SM_INTERNET_H
#endif
