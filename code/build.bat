@echo off

set CommonCompilerFlags=/nologo -fp:fast /MTd -Od -Oi -wd4838

if not exist ..\build mkdir ..\build
pushd ..\build

del *.pdb > NUL 2> NUL
echo WAITING FOR PDB > lock.tmp
cl %CommonCompilerFlags% ..\code\sm_internet.cpp -Fmsm_internet.map -LD /link /incremental:no -opt:ref user32.lib wininet.lib -PDB:internet_%random%.pdb -EXPORT:InitInternet -EXPORT:CloseInternet -EXPORT:CreateInternetRequest -EXPORT:DestroyInternetRequest -EXPORT:InternetRequestGetResults
del lock.tmp
cl %CommonCompilerFlags% /Z7 ..\code\main.cpp /link /incremental:no user32.lib gdi32.lib wininet.lib comctl32.lib

popd
