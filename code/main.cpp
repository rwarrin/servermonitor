#define WIN32_LEAN_AND_MEAN
#define MAJOR_VERSION "0"
#define MINOR_VERSION "0"
#define FIX_VERSION "1"

#include <windows.h>
#include <wininet.h>
#include <stdio.h>
#include <stdlib.h>
#include <commctrl.h>

#include "sm_common.h"
#include "sm_texttools.cpp"

static bool32 GlobalRunning;

static LARGE_INTEGER
Win32GetWallClock(void)
{
	LARGE_INTEGER Result = {0};
	QueryPerformanceCounter(&Result);
	return(Result);
}

static real32
Win32GetSecondsElapsed(LARGE_INTEGER Start, LARGE_INTEGER End)
{
	LARGE_INTEGER Frequency = {0};
	QueryPerformanceFrequency(&Frequency);
	real32 Result = ((real32)(End.QuadPart - Start.QuadPart) /
					 (real32)Frequency.QuadPart);
	return(Result);
}

LRESULT CALLBACK
WindowsCallback(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
	LRESULT Result = 0;

	switch(Message)
	{
		case WM_CLOSE:
		case WM_DESTROY:
		{
			GlobalRunning = false;
			KillTimer(Window, 0);
			PostQuitMessage(0);
		} break;

		case WM_TIMER:
		{
			MessageBox(NULL, "Timer Tick", "Timer", MB_OK);
		} break;

		default:
		{
			Result = DefWindowProc(Window, Message, WParam, LParam);
		} break;
	}

	return (Result);
}

static void
ProcessPendingMessages(void)
{
	MSG Message;
	while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
	{
		switch(Message.message)
		{
			case WM_QUIT:
			{
				GlobalRunning = false;
			} break;

			default:
			{
				TranslateMessage(&Message);
				DispatchMessage(&Message);
			} break;
		}
	}
}

//int WINAPI
//WinMain(HINSTANCE Instance, HINSTANCE PrevInstance, LPSTR CmdLine, int CmdShow)
int main(void)
{
	internet Internet = {0};
	HMODULE InternetLibrary = LoadLibrary("sm_internet.dll");
	if(InternetLibrary)
	{
		Internet.InitInternet = (init_internet *)GetProcAddress(InternetLibrary, "InitInternet");
		Internet.CloseInternet = (close_internet *)GetProcAddress(InternetLibrary, "CloseInternet");
		Internet.CreateInternetRequest = (create_internet_request *)GetProcAddress(InternetLibrary, "CreateInternetRequest");
		Internet.DestroyInternetRequest = (destroy_internet_request *)GetProcAddress(InternetLibrary, "DestroyInternetRequest");
		Internet.InternetRequestGetResults = (internet_request_get_results *)GetProcAddress(InternetLibrary, "InternetRequestGetResults");
	}
	else
	{
		MessageBox(NULL, "Failed to load SM_INTERNET.DLL", "Error", MB_OK);
		return 1;
	}

#if 1
	HINTERNET InternetHandle = Internet.InitInternet();
	struct internet_request_result RequestResult = Internet.CreateInternetRequest(InternetHandle,
																				  "https://www.reddit.com/",
																				  Megabytes(2));
	Internet.InternetRequestGetResults(&RequestResult);
	text_item Searcher = GetString("UpliftingNews", (char *)RequestResult.Data, RequestResult.Length);
	text_item Attribute = GetAttribute("class", (char *)RequestResult.Data, RequestResult.Length, Searcher.Index);
	printf("Found: %.*s at %d\n", Attribute.Length, Attribute.Text, Attribute.Index);
	Internet.DestroyInternetRequest(&RequestResult);
	Internet.CloseInternet(InternetHandle);
#endif

#if 0
	WNDCLASS WindowClass = {0};
	WindowClass.style = CS_HREDRAW | CS_VREDRAW;
	WindowClass.lpfnWndProc = WindowsCallback;
	WindowClass.hInstance = Instance;
	WindowClass.hCursor = (HCURSOR)LoadCursor(Instance, IDC_ARROW);
	WindowClass.lpszClassName = "ServerMonitorClass";

	if(!RegisterClass(&WindowClass))
	{
		MessageBox(NULL, "Failed to register window class.", "Error", MB_OK);
		return 1;
	}

	HWND Window = CreateWindowEx(0,
								 WindowClass.lpszClassName,
								 "Server Monitor v" MAJOR_VERSION "." MINOR_VERSION "." FIX_VERSION,
								 WS_OVERLAPPEDWINDOW | WS_VISIBLE,
								 CW_USEDEFAULT,
								 CW_USEDEFAULT,
								 640, 480,
								 0, 0, Instance, 0);

	if(!Window)
	{
		MessageBox(NULL, "Failed to create window.", "Error", MB_OK);
		return 2;
	}

	int UpdateRateInSeconds = 10;
	//SetTimer(Window, 0, UpdateRateInSeconds*1000, NULL);

#if 1
	GlobalRunning = true;

	real32 TargetFPS = 30.0f;
	real32 TargetSecondsPerFrame = 1.0f / TargetFPS;
	LARGE_INTEGER LastCounter = Win32GetWallClock();
	while(GlobalRunning)
	{
		ProcessPendingMessages();

		LARGE_INTEGER WorkCounter = Win32GetWallClock();
		real32 WorkSecondsElapsed = Win32GetSecondsElapsed(LastCounter, WorkCounter);
		if(WorkSecondsElapsed < TargetSecondsPerFrame)
		{
			DWORD SleepMS = (DWORD)(1000.0f * (TargetSecondsPerFrame - WorkSecondsElapsed));
			if(SleepMS > 0)
			{
				char temp[32] = {0};
				_snprintf(temp, 32, "Sleeping for %d\n", SleepMS);
				OutputDebugString(temp);
				Sleep(SleepMS);
			}
		}

		LastCounter = Win32GetWallClock();
	}
#endif
#endif

	return 0;
}
