#include <windows.h>
#include <wininet.h>

#include "sm_common.h"

extern "C" INIT_INTERNET(InitInternet)
{
	HINTERNET InternetHandle = InternetOpen("Server Monitor",
											INTERNET_OPEN_TYPE_PRECONFIG,
											NULL,
											NULL,
											0);
	return(InternetHandle);
}

extern "C" CLOSE_INTERNET(CloseInternet)
{
	if(InternetHandle)
	{
		InternetCloseHandle(InternetHandle);
	}
}

extern "C" CREATE_INTERNET_REQUEST(CreateInternetRequest)
{
	struct internet_request_result Result = {0};
	HINTERNET RequestHandle = InternetOpenUrl(InternetHandle,
											  Url,
											  NULL,
											  (DWORD)-1L,
											  INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_RELOAD,
											  NULL);
	if(RequestHandle)
	{
		Result.RequestHandle = RequestHandle;
		Result.Data = (void *)VirtualAlloc(0, MaxSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
		if(Result.Data)
		{
			Result.MaxLength = MaxSize;
		}
		else
		{
			InternetCloseHandle(Result.RequestHandle);
			Result.RequestHandle = NULL;
		}
	}

	return(Result);
}

extern "C" DESTROY_INTERNET_REQUEST(DestroyInternetRequest)
{
	if(InternetRequestResult)
	{
		if(InternetRequestResult->Data)
		{
			VirtualFree(InternetRequestResult->Data, 0, MEM_RELEASE);
			InternetRequestResult->MaxLength = -1;
			InternetRequestResult->Length = -1;
		}

		if(InternetRequestResult->RequestHandle)
		{
			InternetCloseHandle(InternetRequestResult->RequestHandle);
		}

		InternetRequestResult->Status = 0;
	}
}

#define BUFFER_SIZE Kilobytes(1)
extern "C" INTERNET_REQUEST_GET_RESULTS(InternetRequestGetResults)
{
	if(RequestResult)
	{
		unsigned char ReadBuffer[BUFFER_SIZE] = {0};
		int InternetStatus = 0;
		DWORD BytesRead = 0;

		while((InternetStatus = InternetReadFile(RequestResult->RequestHandle, ReadBuffer, BUFFER_SIZE, &BytesRead)) &&
			  (BytesRead != 0))
		{
			if(RequestResult->Length + BytesRead >= RequestResult->MaxLength)
			{
				BytesRead = RequestResult->MaxLength - RequestResult->Length;
			}

			CopyMemory((unsigned char *)RequestResult->Data + RequestResult->Length, ReadBuffer, BytesRead);
			RequestResult->Length += BytesRead;
		}
		*((unsigned char *)RequestResult->Data + RequestResult->Length) = 0;
	}
}
#undef BUFFER_SIZE
